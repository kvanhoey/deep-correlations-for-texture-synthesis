""" Main file for Deep Correlation Texture Synthesis.

Code by Kenneth Vanhoey (kenneth@research.kvanhoey.eu).

The code is based on a fork of Chip Huyen's style transfer assignment
code for his course CS20: "TensorFlow for Deep Learning Research"
(cs20.stanford.edu)
"""

import os
#os.environ['TF_CPP_MIN_LOG_LEVEL']='2'
import time
import argparse, sys

import numpy as np
import tensorflow as tf

import psutil
import load_vgg
import utils_images as img
import gram

FLAGS = None

# Weighting coefficients
__CLIP_GRADIENTS__ = False

# Config run options
__ENABLE_SUMMARIES__ = True # Save summaries for tensorboard
__SAVE_IMGS__ = True # Write intermediate states to disk

configProto = tf.ConfigProto(intra_op_parallelism_threads=0,
  inter_op_parallelism_threads=0,
  allow_soft_placement=True,
  log_device_placement=False
)
# configProto = tf.ConfigProto(allow_soft_placement = True)

def get_layers(v='all'):
  return {
    'Gatys': [
      'conv1_1', 'avgpool1',
      'avgpool2',
      'avgpool3',
      'avgpool4',
      'avgpool5'
    ],
    }.get(v,[ # All
    'conv1_1', 'conv1_2', 'avgpool1',
    'conv2_1', 'conv2_2', 'avgpool2',
    'conv3_1', 'conv3_2', 'conv3_3', 'conv3_4', 'avgpool3',
    'conv4_1', 'conv4_2', 'conv4_3', 'conv4_4', 'avgpool4',
    'conv5_1', 'conv5_2', 'conv5_3', 'conv5_4', 'avgpool5',]
    )

def get_local_weights(v='uniform'):
  return {
    'Gatys': [ # From paper "Texture Synthesis Using Convolutional Neural Networks"
      1e9, 1e9,
      1e9,
      1e9,
      1e9,
      1e9
    ],
    'low_level_only': [ # Only low-level features
      1e9, 0., 1e9,
      0., 0., 0.,
      0., 0., 0., 0., 0.,
      0., 0., 0., 0., 0.,
      0., 0., 0., 0., 0.
      ]
    }.get(v,[ # Uniform: all layers equal weights
      1., 1., 1.,
      1., 1., 1.,
      1., 1., 1., 1., 1.,
      1., 1., 1., 1., 1.,
      1., 1., 1., 1., 1.
      ])

class TextureSynthesis(object):

  def __init__(self, source_img, img_width, img_height):
    '''
    img_width and img_height are the dimensions we expect from the generated image.
    We will resize input content image and input source image to match this dimension.
    '''
    seed = 1354
    tf.set_random_seed(seed)
    np.random.seed(seed)

    self.img_width = img_width
    self.img_height = img_height
    self.source_img = img.get_resized_image(source_img, img_width, img_height) - load_vgg.VGG_mean_pixels

    self.initial_img = img.generate_gaussian_noise_image(self.img_width, self.img_height,
      mean=0.0,
      stddev_ratio=1e-3)

    ## create global step (gstep) and hyperparameters for the model
    # Gatys
    self.gram_w = 1.0
    self.gram_layers = get_layers('Gatys')
    self.gram_layer_w = get_local_weights('Gatys')

    self.gstep = tf.Variable(0, dtype=tf.int32, 
                trainable=False, name='global_step')
    decay = tf.train.exponential_decay(FLAGS.LR, self.gstep, 100, 1./1.15, staircase=False)
    boundaries = [1000]
    values = [decay, FLAGS.LR / 5.0]
    self.lr = tf.train.piecewise_constant(self.gstep, boundaries, values)  
    
    filename = FLAGS.input.split("/")[-1]
    self.identifier_string = filename.split(".")[0] + \
          '_Gram-' + str(FLAGS.enable_gram) + \
          '_optimizer-' + FLAGS.optimizer + \
          '_LR-' + str(FLAGS.LR)

    if __ENABLE_SUMMARIES__:
      self.summary_dir = FLAGS.log_dir + 'summary/'
      img.safe_mkdir(self.summary_dir)
    if __SAVE_IMGS__:
      self.synthesized_dir = FLAGS.log_dir + 'synthesized/'
      img.safe_mkdir(self.synthesized_dir)

  def create_input(self):
    '''
    One input_img as a placeholder for source image and generated image.
    Using variable instead of placeholder because it can be optimised.
    '''
    with tf.variable_scope('input') as scope:
      self.input_img = tf.get_variable('in_img', 
                  shape=([1, self.img_height, self.img_width, 3]),
                  dtype=tf.float32,
                  initializer=tf.zeros_initializer(),
                  trainable=True)

  def load_vgg(self):
    '''
    Load the saved model parameters of VGG-19, using the input_img
    as the input to compute the output at each layer of vgg.
    '''
    self.vgg = load_vgg.VGG(self.input_img)
    self.vgg.load()

  def _gram_loss(self, A):
    """ Calculate the total Gram (style) loss as a weighted sum of Gram losses at all style layers."""
    n_layers = len(A)
    E = [gram.loss(A[i], getattr(self.vgg, self.gram_layers[i])) for i in range(n_layers)]
    self.gram_loss = tf.reduce_sum([self.gram_layer_w[i] * E[i] for i in range(n_layers)])

  def losses(self):
    with tf.variable_scope('losses') as scope:
      with tf.Session(config=configProto) as sess:
        sess.run(self.input_img.assign(self.source_img))
        target_activations_gram = sess.run([getattr(self.vgg, layer) for layer in self.gram_layers])

      self.total_loss = 0

      if FLAGS.enable_gram:
        with tf.name_scope('gram_loss'):
          self._gram_loss(target_activations_gram)
          self.total_loss += self.gram_w * self.gram_loss

      def critic(in_x, reuse=False):
        in_x = tf.convert_to_tensor(in_x)
        D1 = tf.layers.conv2d(in_x,   64,  4, 2, 'same', activation=tf.nn.leaky_relu, reuse=reuse, name="critic/c1")
        D2 = tf.layers.conv2d(D1,     128, 4, 2, 'same', activation=tf.nn.leaky_relu, reuse=reuse, name="critic/c2")
        D3 = tf.layers.conv2d(D2,     256, 4, 2, 'same', activation=tf.nn.leaky_relu, reuse=reuse, name="critic/c3")
        D4 = tf.layers.conv2d(D3,     512, 5, 1, 'same', activation=tf.nn.leaky_relu, reuse=reuse, name="critic/c4")
        return tf.layers.conv2d(D4,   1,  1, 1, 'same',  activation=tf.identity,      reuse=reuse, name="critic/proj") # linear projection

      self.img_fake = tf.clip_by_value(self.input_img,0.,1.)
      self.img_true = tf.clip_by_value(self.source_img,0.,1.)

      critic_fake = critic(self.img_fake,False)
      critic_true = critic(self.img_true,True)

      def critic_gradient_norm(a):
        gradients = tf.gradients(critic(a, reuse=True), a)[0]
        return tf.sqrt(tf.reduce_sum(tf.square(gradients), reduction_indices=[1,2,3]))

      k = tf.random.uniform(shape=[1], minval=0.0, maxval=1.0, name="Random_0_1")
      critic_grad_norm = critic_gradient_norm(k*self.img_fake + (1-k)*self.img_true)
      self.critic_grad_loss = tf.losses.mean_squared_error(critic_grad_norm,tf.ones_like(critic_grad_norm))

      self.generator_loss = - tf.reduce_mean(critic_fake)
      self.critic_loss = tf.reduce_mean(critic_fake - critic_true) + 10*self.critic_grad_loss

      self.total_loss = self.generator_loss
      
  def optimize(self):
    self.opt_gen = tf.train.AdamOptimizer(self.lr).minimize(self.generator_loss,self.gstep,var_list=[self.input_img])
    c_vars = [var for var in tf.trainable_variables() if 'losses' in var.name]
    self.opt_critic = tf.train.AdamOptimizer(self.lr).minimize(self.critic_loss,var_list=c_vars)
    self.gstep_increm_op = tf.assign_add(self.gstep,1)
                              

  def create_summary(self):
    with tf.name_scope('summaries'):
      if FLAGS.enable_gram:
        tf.summary.scalar('gram loss', self.gram_loss)
        tf.summary.scalar('gram loss (weighted)', self.gram_w * self.gram_loss)

      tf.summary.scalar('Generator loss', self.generator_loss)
      tf.summary.scalar('Negative critic loss', - self.critic_loss)
      
      tf.summary.image('texture', self.img_fake)
      tf.summary.image('source', self.img_true)
      tf.summary.scalar('mean_texture_diff', tf.losses.mean_squared_error(self.img_fake,self.img_true))
      tf.summary.scalar('_ Learning Rate', self.lr)

      self.summary_op = tf.summary.merge_all()


  def build(self):
    self.create_input()
    print('Load VGG')
    self.load_vgg()
    print('Define losses')
    self.losses()
    print('Define optimizer')
    self.optimize()
    if __ENABLE_SUMMARIES__:
      print('Create summary')
      self.create_summary()

  def train(self, n_iters):
    skip_step = 1

    with tf.Session(config=configProto) as sess:
      ###############################
      print ('Initialize global variables')
      sess.run(tf.global_variables_initializer())
      ###############################
      sess.run(self.input_img.assign(self.initial_img))

      ###############################
      if __ENABLE_SUMMARIES__:
        summaryfile = self.summary_dir + self.identifier_string
        writer = tf.summary.FileWriter(summaryfile, sess.graph)
      ##############################

      total_loss = sess.run(self.total_loss)
      print('   Initial Loss: {:5.1f}'.format(total_loss))

      ## Image saver
      def save_generated_image(idx=0,filename=None):
        # Get values
        gen_image = sess.run(self.input_img)
        if filename is None:
          filename = self.synthesized_dir + self.identifier_string + 'iter{:03d}.png'.format(idx)
        img.save_image(filename, gen_image)

      initial_step = self.gstep.eval()
      
      sess.graph.finalize()
      print ('Start training loop')

      start_time = time.time()
      for index in range(initial_step, n_iters):
        if index >= 5 and index < 20:
          skip_step = 10
        elif index >= 20:
          skip_step = 20

        # Process optimization
        for i in range(5):
          sess.run(self.opt_critic)
        sess.run(self.opt_gen)

        # Report
        if (index + 1) % skip_step == 0:
          print(index)
          if __SAVE_IMGS__:
            save_generated_image(index)

          if __ENABLE_SUMMARIES__:
            total_loss, summary = sess.run([self.total_loss,self.summary_op])

            # Add summary to the summary writer
            writer.add_summary(summary, global_step=index)
            print('   Loss: {:5.3f}'.format(total_loss))
          
      # Save final image
      save_generated_image(filename=self.identifier_string + '_synthesis' + str(FLAGS.max_steps) + '.png')

# =============================================================================
# MAIN
# =============================================================================
def main(_):
  if not tf.gfile.Exists(FLAGS.log_dir):
    tf.gfile.MakeDirs(FLAGS.log_dir)

  print('Training configuration:')
  print('\tGram loss:\t{}'.format(FLAGS.enable_gram))
  print('\tOptimizer: \t{}'.format(FLAGS.optimizer))
  print('\tLR: \t{}\n'.format(FLAGS.LR))
  print('\tLog: \t{}\n'.format(FLAGS.log_dir))

  machine = TextureSynthesis(FLAGS.input, FLAGS.texture_size, FLAGS.texture_size)
  machine.build()
  machine.train(FLAGS.max_steps)

  print ("Finished.\n")

# Argument parser
if __name__ == '__main__':
  parser = argparse.ArgumentParser()
  parser.add_argument('--max_steps', type=int, default=300,
          help='Number of steps to run trainer.')
  parser.add_argument('--LR', type=float, default=3.0,
          help='Learning rate (default 3.0).')
  parser.add_argument('--input', type=str, default='input_images/Texture32.jpg',
          help='Target texture file.')
  parser.add_argument('--texture_size', type=int, default=256,
          help='Requested texture size.')
  parser.add_argument('--learning_rate', type=float, default=2,
          help='Fixed learning rate.')
  parser.add_argument('--optimizer', type=str, default='Adam',help='Optimizer in {\'Adam\' (default), \'LBFGS\'}.')
  parser.add_argument('--disable_gram_loss', dest='enable_gram', action='store_false', default=True, help='Disable Gatys\' style loss (Gram matrices).')
  parser.add_argument('--log_dir', type=str, default='/tmp/DCorrTexSynth/',
          help='Directory for logs and summaries.')
  FLAGS, unparsed = parser.parse_known_args()

tf.app.run(main=main, argv=[sys.argv[0]] + unparsed)
