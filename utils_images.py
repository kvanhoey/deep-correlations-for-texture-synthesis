""" Utils needed for the implementation in TensorFlow
of the paper A Neural Algorithm of Artistic Style (Gatys et al., 2016) 

Created by Chip Huyen (chiphuyen@cs.stanford.edu)
CS20: "TensorFlow for Deep Learning Research"
cs20.stanford.edu
For more details, please read the assignment handout:
https://docs.google.com/document/d/1FpueD-3mScnD0SJQDtwmOb1FrSwo1NGowkXzMwPoLH4/edit?usp=sharing

Edited by Kenneth Vanhoey (kenneth@research.kvanhoey.eu)
"""

import os

from PIL import Image, ImageOps
import numpy as np
import scipy.misc
from six.moves import urllib

def download(download_link, file_name, expected_bytes):
  """ Download the pretrained VGG-19 model if it's not already downloaded """
  if os.path.exists(file_name):
    print("VGG-19 pre-trained model is ready")
    return
  print("Downloading the VGG pre-trained model. This might take a while ...")
  file_name, _ = urllib.request.urlretrieve(download_link, file_name)
  file_stat = os.stat(file_name)
  if file_stat.st_size == expected_bytes:
    print('Successfully downloaded VGG-19 pre-trained model', file_name)
  else:
    raise Exception('File ' + file_name +
            ' might be corrupted. You should try downloading it with a browser.')

def get_resized_image(img_path, width, height, save=True):
  image = Image.open(img_path)
  image = image.convert('RGB')
  # PIL is column major so you have to swap the places of width and height
  image = ImageOps.fit(image, (width, height), Image.ANTIALIAS)
  if save:
    image_dirs = img_path.split('/')
    image_dirs[-1] = 'resized_' + image_dirs[-1]
    out_path = '/'.join(image_dirs)
    if not os.path.exists(out_path):
      image.save(out_path)
  image = np.asarray(image, np.float32)
  return np.expand_dims(image, 0) # add batch dimension [batch, width, height, channels]

def generate_additive_noise_image(base_image, width, height, noise_ratio=0.08):
  noise_image = generate_uniform_noise_image(width, height, 0, noise_ratio)
  return base_image + noise_image

def generate_uniform_noise_image(width, height, mean=0., interval_ratio=0.08):
  return np.random.uniform(mean - interval_ratio, mean + interval_ratio, (1, height, width, 3)).astype(np.float32)

def generate_gaussian_noise_image(width, height, mean=0.5, stddev_ratio=0.1):
  return np.random.normal(mean, stddev_ratio, (1, height, width, 3)).astype(np.float32)

def save_image(path, image):
  if (image.ndim > 3):
    image = image[0]
  image = np.clip(image, 0, 255).astype('uint8')
  scipy.misc.imsave(path, image)

def safe_mkdir(path):
  """ Create a directory if there isn't one already. """
  try:
    os.mkdir(path)
  except OSError:
    pass
