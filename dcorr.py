""" Tensorflow routines to calculate Deep Correlation losses on a layer of activations.
The losses are:
- Deep correlation loss
- Diversity loss
- Smoothness loss
All losses are defined in the paper:
"Deep Correlation for Texture Synthesis"
by Sendik et al., 2017
https://dl.acm.org/citation.cfm?id=3015461

Code by Kenneth Vanhoey (kenneth@research.kvanhoey.eu).
"""
import tensorflow as tf

## Diversity loss ##
def div_loss(a, f):
    """ Diversity loss between two tensors a and f"""
    return tf.reduce_sum((a - f) ** 2)

## Smoothness loss ##
def smooth_loss(g, sigma):
    """ Smoothness loss of a given tensor g"""
    ndims = g.shape.ndims
    if ndims != 4:
        raise ValueError('\'g\' must be 4-dimensional.')
    # The input is a batch of images with shape:
    # [batch, height, width, channels].
    W = g.shape[1]
    H = g.shape[2]

    # 1. Pad with a border of size 1
    padded = tf.image.pad_to_bounding_box(g,1,1,W+2,H+2)

    # 2. Cut shifted slices
    cut_L = padded[:, :-2,1:-1,:]
    cut_R = padded[:,2:  ,1:-1,:]
    cut_B = padded[:,1:-1, :-2,:]
    cut_T = padded[:,1:-1,2:  ,:]
        
    cut_TL = padded[:, :-2, :-2,:]
    cut_TR = padded[:,2:  , :-2,:]
    cut_BL = padded[:, :-2,2:  ,:]
    cut_BR = padded[:,2:  ,2:  ,:]

    # Horizontal differences
    pixel_dif_L = cut_L - g
    pixel_dif_R = cut_R - g
    # Vertical differences
    pixel_dif_T = cut_T - g
    pixel_dif_B = cut_B - g
    # Diagonal differences
    pixel_dif_TL = cut_TL - g
    pixel_dif_BR = cut_BR - g
    pixel_dif_TR = cut_TR - g
    pixel_dif_BL = cut_BL - g

    def inner(A):
        return tf.exp(- sigma * (A ** 2))

    # 4-neighbours
    inner_sum = inner(pixel_dif_L) + inner(pixel_dif_R) + inner(pixel_dif_T) + inner(pixel_dif_B)
    # 8-neighbours
    #inner_sum += inner(pixel_dif_TL) + inner(pixel_dif_TR) + inner(pixel_dif_BL) + inner(pixel_dif_BR)
    inner_sum /= 4

    return tf.reduce_sum(tf.log(inner_sum + 1e-7)) / (2*sigma)

## Deep Correlation loss ##
def _layer_autocorrelation(a):
    """ Calculate autocorrelation of HxWxC matrix with itself in batches (result of each batch is independent, computation is parallel).
    Organisation of a should be NHWC (batches first, channels last).

    A routine called _correlation_matrix (see bottom of this file) provides
    a severely sub-optimal but probably more readable implementation."""
    [B, H, W, C] = a.shape
        
    cross_corr = tf.map_fn( # Applies on each slice along the first dimension (batch) independently
            lambda x : tf.nn.conv2d(
            input = tf.expand_dims(x, 0),  # H,W,C -> 1,H,W,C
            filter= tf.expand_dims(x,-1),  # H,W,C -> H,W,C,1
            strides=[1,1,1,1],
            padding="SAME"
        ),  # Result of conv is 1,H,W,1
        elems=a,
        dtype=tf.float32
    )
    # Cross-cor is of shape (B,1,H,W,1)

    return tf.reshape(cross_corr,[B,H,W])  # B,1,H,W,1 -> B,H,W

def _autocorrelation_normalisation(H, W):
    return _layer_autocorrelation(tf.ones([1,H,W,1]))

def _feature_normalised_autocorrelation(a, normalisation_matrix = None):
    if normalisation_matrix is None:
        normalisation_matrix = _autocorrelation_normalisation(a.shape[1],a.shape[2])

    return _layer_autocorrelation(a) / normalisation_matrix

def _layer_normalised_autocorrelation(a, normalisation_matrix = None):
    if normalisation_matrix is None:
        normalisation_matrix = _autocorrelation_normalisation(a.shape[1],a.shape[2])

    # foreach feature/channel (first dimension), calculate normalised autocorrelation
    [B,H,W,C] = a.shape
    # Move both B (batch) and C (feature channels) to front, and merge into one
    A = tf.reshape(tf.transpose(a,[0,3,1,2]),[B*C,H,W,1])
    # Apply per-feature (first dimension, which here contains both batches and features) autocorrelations
    batched_acorr = _feature_normalised_autocorrelation(A,normalisation_matrix)

    return tf.reshape(batched_acorr,[B,C,H,W])

    # # convert to layer-major (each layer should be indep)
    # A = tf.transpose(a,[0,3,1,2]) # NHWC -> NCHW
    # A = tf.expand_dims(A,-1) # add empty channel layer
    
    # # foreach feature/channel (first dimension), calculate normalised autocorrelation
    # return tf.map_fn(
    #     fn = lambda x: _feature_normalised_autocorrelation(x,normalisation_matrix),
    #     elems=A,
    #     dtype=tf.float32
    # )

def dcorr_loss(a, f):
    """ Calculate the correlation loss at a certain layer
    Inputs:
        a is the feature representation of the source image at that layer
        f is the feature representation of the generated image at that layer
    Output:
        the correlation loss at a certain layer
    """
    with tf.name_scope('CorrelationMatrix_layer'):
        normalisation_matrix = _autocorrelation_normalisation(a.shape[1],a.shape[2])
        A_acorr_norm = _layer_normalised_autocorrelation(a,normalisation_matrix)
        F_acorr_norm = _layer_normalised_autocorrelation(f,normalisation_matrix)

        return tf.reduce_mean((A_acorr_norm - F_acorr_norm) ** 2) / 4

def loss(a, f):
    """ Correlation loss between two feature tensors a and f"""
    return dcorr_loss(a,f)

# /!\ SUBOPTIMAL CODE (provided for readability only, should not be used) ###
def _correlation_matrix(F, W, H):
    """ Create and return the auto-correlation matrix for matrix F of width W and H."""
    F = tf.reshape(F, [W, H])

    W_over_2, H_over_2 = int(W)//2, int(H)//2

    def translated_product(A,i,j=None):
        if j is None: # then its a 1D index.
            index = i
            i, j = tf.floordiv(index,W), tf.floormod(index,W) # convert to 2D index
        i = i - W_over_2
        j = j - H_over_2
            
        def crop(A,i,j):
            start_i = - tf.minimum(i,0) # -min(i,0)
            start_j = - tf.minimum(j,0) # -min(j,0)
            size_i = tf.subtract(W,tf.abs(i))
            size_j = tf.subtract(H,tf.abs(j))
            return tf.slice(A,begin=[start_i,start_j],size=[size_i,size_j])
            
        def w_(i,j):
            return (int(W)-abs(i)) * (int(H)-abs(j))

        with tf.name_scope('TensorProd'):
            P = tf.multiply(crop(A,i,j),crop(A,-i,-j))
            return tf.reduce_sum(P) / w_(i,j)

    R = [translated_product(F,i,j) for i in range(W) for j in range(H)]

    return tf.reshape(R, [W, H])
