""" Tensorflow routines to calculate Gram loss on a layer of activations.
Gram loss is defined and termed "style loss" in the following paper:
"Texture Synthesis Using Convolutional Neural Networks"
by Gatys et al., 2015
https://arxiv.org/abs/1505.07376

Code by Kenneth Vanhoey (kenneth@research.kvanhoey.eu).
"""
import tensorflow as tf

def _gram_matrix(F, M, N):
  """ Create and return the gram matrix for tensor F."""
  F = tf.reshape(F, shape=[-1, M, N])
  return tf.matmul(F, F, transpose_a=True, transpose_b=False)

def loss(a, g):
  """ Calculate the Gram (style) loss at a certain layer
  Inputs:
    a is the feature representation of the source image at that layer
    g is the feature representation of the generated images (batch) at that layer
  Output:
    the style loss at a certain layer (which is E_l in the paper)
  """
  M = int(g.shape[1] * g.shape[2]) # height times width of the feature map  
  N = int(g.shape[3]) # number of filters
  with tf.name_scope('gram_target'):
    A = _gram_matrix(a, M, N)
  with tf.name_scope('gram_input'):
    G = _gram_matrix(g, M, N)
  with tf.name_scope('gram_differences'):
    return tf.reduce_mean((G - A) ** 2 / ((2 * N * M) ** 2))
